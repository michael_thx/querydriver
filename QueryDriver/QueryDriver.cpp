// QueryDriver.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <Windows.h>
#include <SetupAPI.h>
#include <objbase.h>

#include <iostream>
#include <iomanip>

int main()
{
	DWORD Flags(0);

#if 1
	GUID GUID_APO_SETUP_CLASS;
	WCHAR GUID_APO_SETUP_CLASS_STR[] = L"{5989fce8-9cd0-467d-8a6a-5419e31529d4}";
	//WCHAR GUID_APO_SETUP_CLASS_STR[] = L"{5C4C3332-344D-483C-8739-259E934C9CC8}";
	LPCOLESTR lpsz(GUID_APO_SETUP_CLASS_STR);
	LPCLSID pclsid(&GUID_APO_SETUP_CLASS);

	HRESULT resultCLSIDFromString(CLSIDFromString(lpsz, pclsid));
	GUID const* pGUID(&GUID_APO_SETUP_CLASS);
	
#else
	GUID const* pGUID(NULL);
	Flags |= DIGCF_ALLCLASSES;
#endif

	PCWSTR Enumerator(NULL); 
	HWND hwndParent(NULL);
	
	HDEVINFO DeviceInfoSet(SetupDiGetClassDevs(pGUID,
		Enumerator,
		hwndParent,
		Flags));

	if (DeviceInfoSet == INVALID_HANDLE_VALUE)
	{
		std::wcerr << "SetupDiGetClassDevs " << GetLastError() << std::endl;
		LPWSTR lpBuffer(NULL);
		if (!FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
			NULL,
			GetLastError(),
			0,
			(LPWSTR)&lpBuffer,
			0,
			NULL))
		{
			std::wcerr << "FormatMessage failed " << std::hex << GetLastError() << std::endl;
		}
		else
		{
			std::wcerr << "SetupDiGetClassDevs " << lpBuffer << std::endl;
		}
		return 1;
	}

	SP_DEVINFO_DATA DeviceInfoData;
	memset(&DeviceInfoData, 0, sizeof(DeviceInfoData));
	DeviceInfoData.cbSize = sizeof(SP_DEVINFO_DATA);

	for (int DeviceMemberIndex = 0; ; ++DeviceMemberIndex)
	{
		if (!SetupDiEnumDeviceInfo(DeviceInfoSet, DeviceMemberIndex, &DeviceInfoData))
		{
			if (GetLastError() == ERROR_NO_MORE_ITEMS)
			{
				break;
			}
			std::wcerr << "SetupDiEnumDeviceInfo "  << std::hex << GetLastError() << std::endl;
			LPWSTR lpBuffer(NULL);
			if (!FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
				NULL,
				GetLastError(),
				0,
				(LPWSTR)&lpBuffer,
				0,
				NULL))
			{
				std::wcerr << "FormatMessage failed " << std::hex << GetLastError() << std::endl;
			}
			else
			{
				std::wcerr << "SetupDiEnumDeviceInfo " << lpBuffer << std::endl;
			}
			break;
		}

		for (int DriverMemberIndex = 0; ; ++DriverMemberIndex)
		{
			std::wcout << std::dec << DeviceMemberIndex << " - " << DriverMemberIndex << '\r' << std::flush;

			SP_DRVINFO_DATA_V2 DriverInfoData;
			memset(&DriverInfoData, 0, sizeof(DriverInfoData));
			DriverInfoData.cbSize = sizeof(SP_DRVINFO_DATA_V2);

			DWORD DriverType(SPDIT_COMPATDRIVER);

			PSP_DEVINFO_DATA pDeviceInfoData((DriverType == SPDIT_CLASSDRIVER) ? NULL : &DeviceInfoData);
#if 1
			if (!SetupDiBuildDriverInfoList(DeviceInfoSet, pDeviceInfoData, DriverType))
			{
				if (GetLastError() == ERROR_NO_MORE_ITEMS)
				{
					break;
				}
				std::wcerr << "SetupDiBuildDriverInfoList " << std::hex << GetLastError() << std::endl;
				LPWSTR lpBuffer(NULL);
				if (!FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
					NULL,
					GetLastError(),
					0,
					(LPWSTR)&lpBuffer,
					0,
					NULL))
				{
					std::wcerr << "FormatMessage failed " << std::hex << GetLastError() << std::endl;
				}
				else
				{
					std::wcerr << "SetupDiBuildDriverInfoList " << lpBuffer << std::endl;
				}
				break;
			}
#endif
			if (!SetupDiEnumDriverInfo(DeviceInfoSet, pDeviceInfoData, DriverType, DriverMemberIndex, &DriverInfoData))
			{
				if (GetLastError() == ERROR_NO_MORE_ITEMS)
				{
					break;
				}
				std::wcerr << "SetupDiEnumDriverInfo " << std::hex << GetLastError() << std::endl;
				LPWSTR lpBuffer(NULL);
				if (!FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
					NULL,
					GetLastError(),
					0,
					(LPWSTR)&lpBuffer,
					0,
					NULL))
				{
					std::wcerr << "FormatMessage failed " << std::hex << GetLastError() << std::endl;
				}
				else
				{
					std::wcerr << "SetupDiEnumDriverInfo " << lpBuffer << std::endl;
				}
				break;
			}

			std::wstring const mfgName(DriverInfoData.MfgName);

			if (mfgName == L"THX")
			{
				LPOLESTR lpsz;
				StringFromCLSID(DeviceInfoData.ClassGuid, &lpsz);
				std::wcout << std::dec << DeviceMemberIndex << " - " << DriverMemberIndex << " - " << "GUID: " << lpsz << std::endl;
				CoTaskMemFree(lpsz);

				std::wcout << std::dec << DeviceMemberIndex << " - " << DriverMemberIndex << " - " << "DriverType: " << std::hex << DriverInfoData.DriverType << std::endl;
				std::wcout << std::dec << DeviceMemberIndex << " - " << DriverMemberIndex << " - " << "Description: " << DriverInfoData.Description << std::endl;
				std::wcout << std::dec << DeviceMemberIndex << " - " << DriverMemberIndex << " - " << "MfgName: " << DriverInfoData.MfgName << std::endl;
				std::wcout << std::dec << DeviceMemberIndex << " - " << DriverMemberIndex << " - " << "ProviderName: " << DriverInfoData.ProviderName << std::endl;
				std::wcout << std::dec << DeviceMemberIndex << " - " << DriverMemberIndex << " - " << "DriverVersion: " << std::hex << DriverInfoData.DriverVersion << std::endl;
			}
		}
	}

	SetupDiDestroyDeviceInfoList(DeviceInfoSet);

	return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
